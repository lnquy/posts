#!/bin/bash
# Script to start minikube with self-signed TLS key from Docker Private Registry
# @author: Quy Le (lnquy/lngocquy@tma.com.vn)
# @date: June 09 2017
# @version: 0.0.1

# SETUP YOUR VARIABLES HERE
regAddr=192.168.98.100:5000   # Docker Private Registry address [IP:Port] 
k8sAddr=192.168.99.100   # IP address of minikube's Kubernetes server
tlsFile=/root/certs/domain.crt    # Absolute path to self-signed TLS file

# Start
tlsKey=`cat $tlsFile`
certPath="/etc/docker/certs.d/$regAddr"
minikube start

echo "Configuring Docker Private Registry TLS key..."
cmd1=`echo "sudo mkdir -p $certPath"`
caFile=`echo "$certPath/ca.crt"`
tlsKey=`echo "\"$tlsKey\""`
cmd2=`echo "sudo bash -c 'echo $tlsKey > $caFile'"`
# Inside minikube VM
minikube ssh "$cmd1 && $cmd2"
echo "Done!"

