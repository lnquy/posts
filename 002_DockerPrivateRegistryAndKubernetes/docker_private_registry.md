# Docker Private Registry and Kubernetes
```
@author: Quy Le (lnquy/lngocquy@tma.com.vn)  
@date: June 09 2017
@version: 0.0.4
```

### 1. Preface
Docker Private Registry is a separated server like [Docker Hub](https://hub.docker.com/) or [Google Container Registry](https://cloud.google.com/container-registry/) but allows you to manage containers in local private network rather than public it to the Internet.  
The purpose of this guideline is to setup and configure a Docker Private Registry that can be used generally by clients in local network and Kubernetes (single node on minikube).

 - The system and versions in this guideline: 
    ```
    - OS: Ubuntu 16.10 Yakkety Yak (amd64)
    - Docker: 17.03.1-ce (Community Edition)
    - Kubectl: 1.6
    - Minikube: 0.19.1
    ```
 - Let assume that you already installed [docker](https://docs.docker.com/engine/installation/linux/ubuntu/), [minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/) and [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) (for running Kubernetes in local single node) on your system and can run/deploy local containers with no issues.
 - Let assume that the minikube Kubernetes server is running on `192.168.99.100/24` address and the Docker Private Registry will be setup to run on `192.168.98.100/24`.
 - **All commands executed in super user `sudo` mode.**  
   **You should start a terminal with `sudo -i` first to be able to copy/paste all commands in this guideline.**
 
 ### 2. Setup Docker Private Registry  
 #### 2.1. Pull Docker Private Registry image
```
docker pull registry
```
This command will pull the `latest` version of registry image from Docker Hub to your local system.  
You can specify the version of image by `docker pull registry:[version_tag]`. At the time writing this document, the latest version of registry image is `registry:2`.  
Run `docker images` to verify pulled image.  

#### 2.2. Add CA name for registry server's address
```
vi /etc/ssl/openssl.cnf
    -> Add the line "subjectAltName=IP:192.168.98.100" to the [ v3_ca ] section like below:
    
    ...
    [ v3_ca ]
    subjectAltName=IP:192.168.98.100
    ...
    
Save and exit.
```
The command above add an alternative name for IP address 192.168.98.100 so we can generate the TLS key for that address instead of using FQDN.  

#### 2.3. Generate TLS key for registry server
```
cd ~/
mkdir -p certs
openssl req -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key -x509 -days 365 -out certs/domain.crt
```
Enter information to generate TLS key.  
**Note: Be sure to use the name `192.168.98.100` (the address of registry server) as the FQDN field.**  
After done, two files `domain.key` and `domain.crt` will be placed in `$HOME/certs` folder.
 
#### 2.4. Trust self-signed TLS key  
Copy the self-signed TLS `domain.crt` to `/etc/docker/certs.d/*`
 ```
 mkdir -p /etc/docker/certs.d/192.168.98.100:5000
 cp $HOME/certs/domain.crt /etc/docker/certs.d/192.168.98.100:5000/ca.crt
 update-ca-certificates
 ```
 Optional, you may need to restart docker service after this step `service docker restart`.    
**Note: This step must be repeatedly done in all clients in the network.**  

#### 2.5. Create a Docker network for registry server  
```
docker network create --subnet 192.168.98.0/24 --gateway 192.168.98.1 ipstatic_98
```
This command creates a local bridge network named `ipstatic_98`.  
Run `docker network ls` to verify new created network.  
 
#### 2.6. Start Docker Private Registry server
```
docker run -d -p 5000:5000 --net=ipstatic_98 --ip=192.168.98.100 --restart=always --name=docker-private-registry \
 -v $HOME/certs:/certs \
 -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
 -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
 registry:latest
```
This command will start the `registry:latest` image in a new container in `ipstatic_98` subnet and expose the `192.168.98.100:5000` address. The HTTP connection to container will be secured by our self-signed TLS above.  
Run `docker ps` to verify registry container has been started.  

 #### 2.7. Verify Docker Private Registry server
For example, we have a local Docker image as below:
```
root@lnquy:/home/lnquy# docker images | grep dtcloud
REPOSITORY                          TAG                 IMAGE ID            CREATED             SIZE
dtcloud_thing                       0.1                 6f0308f05f7b        17 hours ago        15.8 MB
```
Tag this image with our Docker Private Registry repository:
```
root@lnquy:/home/lnquy# docker tag 6f0308f05f7b 192.168.98.100:5000/dtcloud_thing:0.1
root@lnquy:/home/lnquy# docker images | grep dtcloud
REPOSITORY                          TAG                 IMAGE ID            CREATED             SIZE
192.168.98.100:5000/dtcloud_thing   0.1                 6f0308f05f7b        18 hours ago        15.8 MB
dtcloud_thing                       0.1                 6f0308f05f7b        18 hours ago        15.8 MB
```
Push tagged image to registry:
```
docker push 192.168.98.100:5000/dtcloud_thing:0.1
```
If no errors occurred then we successfully setup the Docker Private Registry server.  
Try to pull image from registry to our local machine:
```
docker pull 192.168.98.100:5000/dtcloud_thing:0.1
```

 ### 3. Kubernetes and Docker Private Registry
#### 3.1. Trust self-signed TLS key
- Add your self-signed TLS key to Kubernetes VM:
```
minikube start
minikube ssh
sudo mkdir -p /etc/docker/certs.d/192.168.98.100:5000
sudo cd /etc/docker/certs.d/192.168.98.100:5000
sudo vi ca.crt
    -> Copy your $HOME/certs/domain.crt content to this new file and save it.
exit
```
- **Note: This process must be done repeatedly each time you start minikube since the created certification file will be deleted when stopping minikube.**  
**You can automate this process by running the `mnk` script (provided along with this guideline) instead of running `minikube start` as below.**
```
-> Edit the "regAddr, k8sAddr and tlsFile" variables inside mnk.sh script file first.

sudo cp [path_to_mnk.sh_file] /usr/bin/mnk
sudo chmod +x /usr/bin/mnk
mnk
```  

#### 3.2. Pull images from Docker Private Registry to Kubernetes
 **Note: From this step, you can run with normal account instead of super user.**  
 - Start minikube as normal: `minikube start`.
 - Open Kubernetes dashboard: `minikube dashboard`.
 - Click `Create` button (top right corner).
 - Set app name: `dtcloud_thing`.
 - Set container image: `192.168.98.100:5000/dtcloud_thing:0.1`.
 - Click `Deploy`.  
 - Verify Kubernetes successfully pull images from local registry server.
 
 ### 4. Troubleshooting  
 #### 4.1. If you cannot ping the registry server (192.168.98.100):
- Check the docker network `ipstatic_98`.
- Ping `ipstatic_98` default gateway (192.168.98.1).
- Verify registry container has been started or not?
- Use `docker ps` to verify the PORTS field has value `0.0.0.0:5000->5000/tcp`.
- If none of the above works, then stop and delete container and try to re-run the "Start Docker Private Registry server" command.  

 #### 4.2. If you issued any problems with TLS certification or authentication while pushing/pulling image:
- Check the self-signed TLS key generation process (Is information correct?).
- Make sure the client trusted that self-signed TLS key.
- Make sure registry container has been started with secure TLS parameters.
- If none of the above works, then you may try to re-generate TLS key and add that key to clients again.

#### 4.3. If you can push/pull images between local Docker and Docker registry but Kubernetes cannot pull images from registry and return error "x509: certificate signed by unknown authority":
- Check the self-signed TLS key generation process (Is information correct?).
- Verify step 3.1.
 
 ### 5. Complete terminal
```
lnquy@lnquy:~$ sudo -i
[sudo] password for lnquy: 
root@lnquy:~# 
root@lnquy:~# docker pull registry
Using default tag: latest
latest: Pulling from library/registry
79650cf9cc01: Already exists 
70ce42745103: Already exists 
77edd1a7fa4d: Already exists 
432773976ace: Already exists 
3234a47fe5a9: Already exists 
Digest: sha256:a3551c422521617e86927c3ff57e05edf086f1648f4d8524633216ca363d06c2
Status: Downloaded newer image for registry:latest
root@lnquy:~# 
root@lnquy:~# 
root@lnquy:~# docker images
REPOSITORY             TAG                 IMAGE ID            CREATED             SIZE
registry               latest              9d0c4eabab4d        3 weeks ago         33.2 MB
centurylink/ca-certs   latest              ec29b98d130f        2 years ago         258 kB
root@lnquy:~# 
root@lnquy:~# 
root@lnquy:~# vi /etc/ssl/openssl.cnf
[...Add subjectAltName=IP:192.168.98.100 to [ v3_ca ] section...]
root@lnquy:~# 
root@lnquy:~# 
root@lnquy:~# cd ~/
root@lnquy:~# mkdir -p certs
root@lnquy:~# openssl req -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key -x509 -days 365 -out certs/domain.crt
Generating a 4096 bit RSA private key
....................................................................................................................................................................................++
.........................................................................................................................................................................................................................................................++
writing new private key to 'certs/domain.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:VN
State or Province Name (full name) [Some-State]:Ho Chi Minh City
Locality Name (eg, city) []:Ho Chi Minh City
Organization Name (eg, company) [Internet Widgits Pty Ltd]:TMA Solutions
Organizational Unit Name (eg, section) []:Disruptive Software        
Common Name (e.g. server FQDN or YOUR name) []:192.168.98.100
Email Address []:lngocquy@tma.com.vn
root@lnquy:~# 
root@lnquy:~# 
root@lnquy:~# mkdir -p /etc/docker/certs.d/192.168.98.100:5000
root@lnquy:~# cp $HOME/certs/domain.crt /etc/docker/certs.d/192.168.98.100:5000/ca.crt
root@lnquy:~# update-ca-certificates
Updating certificates in /etc/ssl/certs...
0 added, 0 removed; done.
Running hooks in /etc/ca-certificates/update.d...
done.
root@lnquy:~# 
root@lnquy:~# 
root@lnquy:~# docker network create --subnet 192.168.98.0/24 --gateway 192.168.98.1 ipstatic_98
9a81fea20b638e9a6e56b7c675da8d60110dc145db87ca065ead0be53dd9e614
root@lnquy:~# docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
dc80821f210c        bridge              bridge              local
778c2b468e7c        host                host                local
9a81fea20b63        ipstatic_98         bridge              local
ed8fcd6c80f8        none                null                local
root@lnquy:~# 
root@lnquy:~# 
root@lnquy:~# docker run -d -p 5000:5000 --net=ipstatic_98 --ip=192.168.98.100 --restart=always --name=docker-private-registry \
>  -v $HOME/certs:/certs \
>  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
>  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
>  registry:latest
ebf0105b635ea65dbfd06dbc52d15a2c91dabd8d401b6072d5d1bac2723d1463
root@lnquy:~# 
root@lnquy:~# 
root@lnquy:~# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
ebf0105b635e        registry:latest     "/entrypoint.sh /e..."   8 seconds ago       Up 6 seconds        0.0.0.0:5000->5000/tcp   docker-private-registry
root@lnquy:~# 
root@lnquy:~# 
root@lnquy:~# cd /home/lnquy/workspace/go/src/bitbucket.org/disruptive-technologies/thing
root@lnquy:/home/lnquy/workspace/go/src/bitbucket.org/disruptive-technologies/thing# docker image build -t dtcloud_thing:0.1 ./
Sending build context to Docker daemon   133 MB
Step 1/5 : FROM centurylink/ca-certs
 ---> ec29b98d130f
Step 2/5 : EXPOSE 8000
 ---> Running in c337f194e7db
 ---> c038f8bffbde
Removing intermediate container c337f194e7db
Step 3/5 : EXPOSE 9102
 ---> Running in d460e32433c4
 ---> a951a18707c2
Removing intermediate container d460e32433c4
Step 4/5 : COPY thing.bin /thing
 ---> 80ebb7eaf595
Removing intermediate container 22f3b3f6942f
Step 5/5 : ENTRYPOINT /thing
 ---> Running in 6a595b459c53
 ---> 7b5ff27eaa9c
Removing intermediate container 6a595b459c53
Successfully built 7b5ff27eaa9c
root@lnquy:/home/lnquy/workspace/go/src/bitbucket.org/disruptive-technologies/thing# 
root@lnquy:/home/lnquy/workspace/go/src/bitbucket.org/disruptive-technologies/thing# 
root@lnquy:/home/lnquy/workspace/go/src/bitbucket.org/disruptive-technologies/thing# cd ~
root@lnquy:~# docker images
REPOSITORY             TAG                 IMAGE ID            CREATED             SIZE
dtcloud_thing          0.1                 7b5ff27eaa9c        7 seconds ago       15.8 MB
registry               latest              9d0c4eabab4d        3 weeks ago         33.2 MB
centurylink/ca-certs   latest              ec29b98d130f        2 years ago         258 kB
root@lnquy:~# 
root@lnquy:~# 
root@lnquy:~# docker tag 7b5ff27eaa9c 192.168.98.100:5000/dtcloud_thing:0.1
root@lnquy:~# docker images
REPOSITORY                          TAG                 IMAGE ID            CREATED             SIZE
192.168.98.100:5000/dtcloud_thing   0.1                 7b5ff27eaa9c        3 minutes ago       15.8 MB
dtcloud_thing                       0.1                 7b5ff27eaa9c        3 minutes ago       15.8 MB
registry                            latest              9d0c4eabab4d        3 weeks ago         33.2 MB
centurylink/ca-certs                latest              ec29b98d130f        2 years ago         258 kB
root@lnquy:~# 
root@lnquy:~# 
root@lnquy:~# docker push 192.168.98.100:5000/dtcloud_thing:0.1
The push refers to a repository [192.168.98.100:5000/dtcloud_thing]
3ff85d46ba81: Pushed 
0cfde93eba7d: Pushed 
5f70bf18a086: Pushed 
0.1: digest: sha256:88ad33b2a9013cbe7ce13e80841a4a5f3bb6da9f63ec4002a2d5ad2a7710bf97 size: 944
root@lnquy:~# 
root@lnquy:~# 
root@lnquy:~# docker pull 192.168.98.100:5000/dtcloud_thing:0.1
0.1: Pulling from dtcloud_thing
Digest: sha256:88ad33b2a9013cbe7ce13e80841a4a5f3bb6da9f63ec4002a2d5ad2a7710bf97
Status: Image is up to date for 192.168.98.100:5000/dtcloud_thing:0.1
root@lnquy:~# 
root@lnquy:~# 
root@lnquy:~# cp /home/lnquy/Download/mnk.sh /usr/bin/mnk
root@lnquy:~# chmod +x /usr/bin/mnk
root@lnquy:~# mnk
Starting local Kubernetes v1.6.4 cluster...
Starting VM...
Moving files into cluster...
Setting up certs...
Starting cluster components...
Connecting to cluster...
Setting up kubeconfig...
Kubectl is now configured to use the cluster.
Configuring Docker Private Registry TLS key...
Done!
```
