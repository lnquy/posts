## Result

```
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ go test -v
=== RUN   TestIsCountryCodeByContains
--- PASS: TestIsCountryCodeByContains (0.00s)
=== RUN   TestIsCountryCodeByHashMap
--- PASS: TestIsCountryCodeByHashMap (0.00s)
=== RUN   TestIsCountryCodeByCantorPairing
--- PASS: TestIsCountryCodeByCantorPairing (0.00s)
PASS
ok  	github.com/lnquy/posts/001_CountryCode	0.002s
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ 
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ 
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ go test -cpu=1 -bench=.
BenchmarkIsCountryCodeByContains      	100000000	        10.4 ns/op
BenchmarkIsCountryCodeByHashMap       	100000000	        12.5 ns/op
BenchmarkIsCountryCodeByCantorPairing 	200000000	         6.95 ns/op
PASS
ok  	github.com/lnquy/posts/001_CountryCode	4.418s
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ 
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ 
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ go test -cpu=1 -bench=.
BenchmarkIsCountryCodeByContains      	10000000	       144 ns/op
BenchmarkIsCountryCodeByHashMap       	100000000	        12.3 ns/op
BenchmarkIsCountryCodeByCantorPairing 	100000000	        10.9 ns/op
PASS
ok  	github.com/lnquy/posts/001_CountryCode	3.945s
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ 
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ 
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ go test -cpu=1 -bench=.
BenchmarkIsCountryCodeByContains      	10000000	       206 ns/op
BenchmarkIsCountryCodeByHashMap       	30000000	        55.9 ns/op
BenchmarkIsCountryCodeByCantorPairing 	20000000	        72.0 ns/op
PASS
ok  	github.com/lnquy/posts/001_CountryCode	5.529s
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ 
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ 
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ go test -cpu=1 -bench=BenchmarkIsCountryCodeByHashMap2
BenchmarkIsCountryCodeByHashMap2 	 5000000	       276 ns/op
PASS
ok  	github.com/lnquy/posts/001_CountryCode	1.668s
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ 
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ 
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ go test -cpu=1 -bench=BenchmarkIsCountryCodeByCantorPairing2
BenchmarkIsCountryCodeByCantorPairing2 	10000000	       222 ns/op
PASS
ok  	github.com/lnquy/posts/001_CountryCode	2.462s
lnquy@lnquy:~/workspace/go/src/github.com/lnquy/posts/001_CountryCode$ 

```
