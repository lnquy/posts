package main

import (
	"strings"
)

var (
	countryCodes string = "ad,ae,af,ag,ai,al,am,an,ao,aq,ar,as,at,au,aw,ax,az,ba,bb,bd,be,bf,bg,bh,bi,bj,bl,bm,bn,bo," +
		"br,bs,bt,bv,bw,by,bz,ca,cc,cd,cf,cg,ch,ci,ck,cl,cm,cn,co,cr,cu,cv,cx,cy,cz,de,dj,dk,dm,do,dz,ec,ee,eg,eh,er," +
		"es,et,fi,fj,fk,fm,fo,fr,ga,gb,gd,ge,gf,gg,gh,gi,gl,gm,gn,gp,gq,gr,gs,gt,gu,gw,gy,hk,hm,hn,hr,ht,hu,id,ie,il," +
		"im,in,io,iq,ir,is,it,je,jm,jo,jp,ke,kg,kh,ki,km,kn,kp,kr,kw,ky,kz,la,lb,lc,li,lk,lr,ls,lt,lu,lv,ly,ma,mc,md," +
		"me,mf,mg,mh,mk,ml,mm,mn,mo,mp,mq,mr,ms,mt,mu,mv,mw,mx,my,mz,na,nc,ne,nf,ng,ni,nl,no,np,nr,nu,nz,om,pa,pe,pf," +
		"pg,ph,pk,pl,pm,pn,pr,ps,pt,pw,py,qa,re,ro,rs,ru,rw,sa,sb,sc,sd,se,sg,sh,si,sj,sk,sl,sm,sn,so,sr,ss,st,sv,sy," +
		"sz,tc,td,tf,tg,th,tj,tk,tl,tm,tn,to,tr,tt,tv,tw,tz,ua,ug,um,us,uy,uz,va,vc,ve,vg,vi,vn,vu,wf,ws,ye,yt,za,zm,zw"
	ccMap map[string]bool = make(map[string]bool)
	ccArr []uint16           = make([]uint16, 247)
)

func init() {
	initCCHashMap()
	initCCArray()
}

func initCCHashMap() {
	ccs := strings.Split(countryCodes, ",")
	for _, cc := range ccs {
		ccMap[cc] = true
	}
}

func initCCArray() {
	ccs := strings.Split(countryCodes, ",")
	for i, cc := range ccs {
		ccArr[i] = cantorPair(cc)
	}

	// Sort
	for i := 0; i < len(ccArr)-1; i++ {
		for j := len(ccArr) - 1; j > i; j-- {
			if ccArr[i] > ccArr[j] {
				ccArr[i], ccArr[j] = ccArr[j], ccArr[i]
			}
		}
	}
}

func cantorPair(input string) uint16 {
	k1 := uint16(input[0])
	k2 := uint16(input[1])
	return uint16(((k1+k2)*(k1+k2+1))/2 + k2)
}

func isCountryCodeByContains(input string) bool {
	return strings.Contains(countryCodes, input)
}

func isCountryCodeByHashMap(input string) bool {
	_, ok := ccMap[input]
	return ok
}

func isCountryCodeByCantorPairing(input string) bool {
	p := cantorPair(input)

	head := 0
	tail := len(ccArr) - 1
	for head <= tail {
		median := (head + tail) / 2
		if ccArr[median] == p {
			return true
		} else if ccArr[median] < p {
			head = median + 1
		} else if ccArr[median] > p {
			tail = median - 1
		}
	}
	return false
}

func main() {

}
