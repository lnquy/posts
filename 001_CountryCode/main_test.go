package main

import (
	"testing"
	"math/rand"
	"time"
	"strings"
)

type TestCase struct {
	Input  string
	Output bool
}

var testCases []TestCase = []TestCase{
	{"ad", true},
	{"ae", true},
	{"af", true},
	{"ag", true},
	{"ai", true},
	{"al", true},
	{"am", true},
	{"an", true},
	{"ao", true},
	{"aq", true},
	{"ar", true},
	{"as", true},
	{"at", true},
	{"au", true},
	{"aw", true},
	{"ax", true},
	{"az", true},
	{"ba", true},
	{"bb", true},
	{"bd", true},
	{"be", true},
	{"bf", true},
	{"bg", true},
	{"bh", true},
	{"bi", true},
	{"bj", true},
	{"bl", true},
	{"bm", true},
	{"bn", true},
	{"bo", true},
	{"br", true},
	{"bs", true},
	{"bt", true},
	{"bv", true},
	{"bw", true},
	{"by", true},
	{"bz", true},
	{"ca", true},
	{"cc", true},
	{"cd", true},
	{"cf", true},
	{"cg", true},
	{"ch", true},
	{"ci", true},
	{"ck", true},
	{"cl", true},
	{"cm", true},
	{"cn", true},
	{"co", true},
	{"cr", true},
	{"cu", true},
	{"cv", true},
	{"cx", true},
	{"cy", true},
	{"cz", true},
	{"de", true},
	{"dj", true},
	{"dk", true},
	{"dm", true},
	{"do", true},
	{"dz", true},
	{"ec", true},
	{"ee", true},
	{"eg", true},
	{"eh", true},
	{"er", true},
	{"es", true},
	{"et", true},
	{"fi", true},
	{"fj", true},
	{"fk", true},
	{"fm", true},
	{"fo", true},
	{"fr", true},
	{"ga", true},
	{"gb", true},
	{"gd", true},
	{"ge", true},
	{"gf", true},
	{"gg", true},
	{"gh", true},
	{"gi", true},
	{"gl", true},
	{"gm", true},
	{"gn", true},
	{"gp", true},
	{"gq", true},
	{"gr", true},
	{"gs", true},
	{"gt", true},
	{"gu", true},
	{"gw", true},
	{"gy", true},
	{"hk", true},
	{"hm", true},
	{"hn", true},
	{"hr", true},
	{"ht", true},
	{"hu", true},
	{"id", true},
	{"ie", true},
	{"il", true},
	{"im", true},
	{"in", true},
	{"io", true},
	{"iq", true},
	{"ir", true},
	{"is", true},
	{"it", true},
	{"je", true},
	{"jm", true},
	{"jo", true},
	{"jp", true},
	{"ke", true},
	{"kg", true},
	{"kh", true},
	{"ki", true},
	{"km", true},
	{"kn", true},
	{"kp", true},
	{"kr", true},
	{"kw", true},
	{"ky", true},
	{"kz", true},
	{"la", true},
	{"lb", true},
	{"lc", true},
	{"li", true},
	{"lk", true},
	{"lr", true},
	{"ls", true},
	{"lt", true},
	{"lu", true},
	{"lv", true},
	{"ly", true},
	{"ma", true},
	{"mc", true},
	{"md", true},
	{"me", true},
	{"mf", true},
	{"mg", true},
	{"mh", true},
	{"mk", true},
	{"ml", true},
	{"mm", true},
	{"mn", true},
	{"mo", true},
	{"mp", true},
	{"mq", true},
	{"mr", true},
	{"ms", true},
	{"mt", true},
	{"mu", true},
	{"mv", true},
	{"mw", true},
	{"mx", true},
	{"my", true},
	{"mz", true},
	{"na", true},
	{"nc", true},
	{"ne", true},
	{"nf", true},
	{"ng", true},
	{"ni", true},
	{"nl", true},
	{"no", true},
	{"np", true},
	{"nr", true},
	{"nu", true},
	{"nz", true},
	{"om", true},
	{"pa", true},
	{"pe", true},
	{"pf", true},
	{"pg", true},
	{"ph", true},
	{"pk", true},
	{"pl", true},
	{"pm", true},
	{"pn", true},
	{"pr", true},
	{"ps", true},
	{"pt", true},
	{"pw", true},
	{"py", true},
	{"qa", true},
	{"re", true},
	{"ro", true},
	{"rs", true},
	{"ru", true},
	{"rw", true},
	{"sa", true},
	{"sb", true},
	{"sc", true},
	{"sd", true},
	{"se", true},
	{"sg", true},
	{"sh", true},
	{"si", true},
	{"sj", true},
	{"sk", true},
	{"sl", true},
	{"sm", true},
	{"sn", true},
	{"so", true},
	{"sr", true},
	{"ss", true},
	{"st", true},
	{"sv", true},
	{"sy", true},
	{"sz", true},
	{"tc", true},
	{"td", true},
	{"tf", true},
	{"tg", true},
	{"th", true},
	{"tj", true},
	{"tk", true},
	{"tl", true},
	{"tm", true},
	{"tn", true},
	{"to", true},
	{"tr", true},
	{"tt", true},
	{"tv", true},
	{"tw", true},
	{"tz", true},
	{"ua", true},
	{"ug", true},
	{"um", true},
	{"us", true},
	{"uy", true},
	{"uz", true},
	{"va", true},
	{"vc", true},
	{"ve", true},
	{"vg", true},
	{"vi", true},
	{"vn", true},
	{"vu", true},
	{"wf", true},
	{"ws", true},
	{"ye", true},
	{"yt", true},
	{"za", true},
	{"zm", true},
	{"zw", true},

	{"da", false},
	{"wz", false},
	{"ay", false},
	{"ty", false},
	{"nv", false},
	{"cw", false},
	{"ef", false},
	{"jf", false},
	{"ok", false},
	{"ll", false},
	{"vv", false},
	{"zz", false},
}

func TestIsCountryCodeByContains(t *testing.T) {
	for _, tc := range testCases {
		res := isCountryCodeByContains(tc.Input)
		if res != tc.Output {
			t.Errorf("isCountryCodeByContains(%s) = %v, expected %v", tc.Input, res, tc.Output)
		}
	}
}

func TestIsCountryCodeByHashMap(t *testing.T) {
	for _, tc := range testCases {
		res := isCountryCodeByHashMap(tc.Input)
		if res != tc.Output {
			t.Errorf("isCountryCodeByHashMap(%s) = %v, expected %v", tc.Input, res, tc.Output)
		}
	}
}

func TestIsCountryCodeByCantorPairing(t *testing.T) {
	for _, tc := range testCases {
		res := isCountryCodeByCantorPairing(tc.Input)
		if res != tc.Output {
			t.Errorf("isCountryCodeByCantorPairing(%s) = %v, expected %v", tc.Input, res, tc.Output)
		}
	}
}

var (
	resTmp   bool
	tmpCCArr []string = strings.Split(countryCodes, ",")
)

func BenchmarkIsCountryCodeByContains(b *testing.B) {
	rand.Seed(time.Now().Unix())
	for i := 0; i < b.N; i++ {
		//resTmp = isCountryCodeByContains("ad") // Best case
		//resTmp = isCountryCodeByContains("zz") // Worst case
		resTmp = isCountryCodeByContains(tmpCCArr[rand.Intn(247)]) // Random from [0, 247)
	}
}

func BenchmarkIsCountryCodeByHashMap(b *testing.B) {
	rand.Seed(time.Now().Unix())
	for i := 0; i < b.N; i++ {
		//resTmp = isCountryCodeByHashMap("py") // Average case
		resTmp = isCountryCodeByHashMap(tmpCCArr[rand.Intn(247)]) // Random from [0, 247)
	}
}

func BenchmarkIsCountryCodeByCantorPairing(b *testing.B) {
	rand.Seed(time.Now().Unix())
	for i := 0; i < b.N; i++ {
		//resTmp = isCountryCodeByCantorPairing("kw") // Best case
		//resTmp = isCountryCodeByCantorPairing("zz") // Worst case
		resTmp = isCountryCodeByCantorPairing(tmpCCArr[rand.Intn(247)]) // Random from [0, 247)
	}
}

// Cantor pairing solution has better result in both worst case and best case than hash map solution.
// But when testing with rand index (to simulate real life cases), the result of hash map solution is better than Cantor
// pairing solution. But why is that possible?!?
// One college of mine proposed a theory about Go's map may have a mechanism to retain hashed values. So in the cases of
// randomize duplicated input, Go map just have to check the cached rather than re-hash input.
// To examine this theory, we defined a unique index list and benchmark again.
// The result is Cantor pairing is better than hash map when there's no duplication of input.
// TODO: This seems a very interesting problem, need to dive deep into this =)
var idx []byte = []byte{0, 50, 100, 134, 60, 33, 45, 23, 86, 77, 44, 200, 233, 123, 111, 66, 43, 44, 55, 88, 99, 11, 22, 33}

func BenchmarkIsCountryCodeByHashMap2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, tc := range idx {
			resTmp = isCountryCodeByHashMap(tmpCCArr[tc])
		}
	}
}

func BenchmarkIsCountryCodeByCantorPairing2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, tc := range idx {
			resTmp = isCountryCodeByCantorPairing(tmpCCArr[tc])
		}
	}
}

// Another benchmark for understanding benchmark result in random cases of Hash Map and Cantor Pairing solution
func BenchmarkIsCountryCodeByHashMap3(b *testing.B) {
	var idx int
	for i := 0; i < b.N; i++ {
		idx = i % 500
		if idx > 246 {
			resTmp = isCountryCodeByHashMap("zz")
		} else {
			resTmp = isCountryCodeByHashMap(tmpCCArr[idx])
		}
	}
}

func BenchmarkIsCountryCodeByCantorPairing3(b *testing.B) {
	var idx int
	for i := 0; i < b.N; i++ {
		idx = i % 500
		if idx > 246 {
			resTmp = isCountryCodeByCantorPairing("zz")
		} else {
			resTmp = isCountryCodeByCantorPairing(tmpCCArr[idx])
		}
	}
}
